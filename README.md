
## Task
- Create a page with a filter and a table, to present the consumed data
- The filter should include only one text input field to filter by country code
- The table should include only 2 columns: `country name` and `country code`
- Styling is up to you
- Test your solution
- Use typescript

## How to run app?

* Download repo
* run `npm i`
* run `npm start`

### How to test app?
* run `npm i`
    * Only needed if you have not ran `npm i` before
* run `npm run test`

## Stack
* React
* TypeScript
* MUI
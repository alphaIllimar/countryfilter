import { Divider, Typography } from "@mui/material";
import { phrases } from "./helpers/phrases";
import styles from './App.module.scss';
import Filter from "./components/Filter/Filter";
import { CountryProvider } from "./providers/CountryProvider/CountryProvider";
import { CountryList } from "./components/CountryList/CountryList";
import { IState, useState } from "./react-hooks/useState";

export default function App() {
  const _appliedFilter:IState<string> = useState<string>('');

  return (
    <div className={styles.container}>
      <CountryProvider>
        <Typography 
          variant="h1"
          color='primary'
        >
          {phrases.heading}
        </Typography>
        <Divider />
        <Filter
          onChange={(filter:string) => {
            _appliedFilter.set(filter);
          }}
        />
        <CountryList filter={_appliedFilter.value} />
      </CountryProvider>
    </div>
  );
}

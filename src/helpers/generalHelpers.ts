
/**
 * Does a millisecond delay for a async function
 * @param ms The milliseconds you want to delay
 */
export const delay = (ms:number):Promise<void> => new Promise(res => setTimeout(res, ms));


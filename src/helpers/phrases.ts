/**
 * Holds all the translations of the application
 */
export const phrases = {
    heading: "Countries",
    search: 'Search',
    code: 'Country Code',
    listCaption: 'Country List'
};
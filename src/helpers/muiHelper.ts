
import { createTheme, Theme, ThemeOptions } from '@mui/material/styles';
import { etEE } from '@mui/material/locale';


/**
 * The MUI Theme options for AlphaPro EE
 */
export const g_appThemeOptions:ThemeOptions = {
    typography: {
        fontFamily: [            
            'Montserrat',  
            'Roboto',          
            'Arial',
            'Helvetica',
            'sans-serif'
        ].join(', '),
        h1: {
            fontSize: '24px'
        }
    }  
};
/**
 * The theme for the application
 */
export const g_AppTheme:Theme = createTheme(g_appThemeOptions, etEE);
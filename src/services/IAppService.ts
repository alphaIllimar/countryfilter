import { ICountry } from "../models/ICountry";

export interface IAppService {
    /**
     * Fetches all the countries available to the system
     */
    getCountries:() => Promise<ICountry[]>
}
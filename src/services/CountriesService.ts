import { ICountry } from "../models/ICountry";
import { IAppService } from "./IAppService";

const ENDPOINT:string = 'https://countries.trevorblades.com/';

export class CountriesService implements IAppService {
    /**
     * Returns the response object from the fetch result
     * @param res The fetch result
     */
    private async _getResponse(res:Response):Promise<any> {

        const responseText:string = await res.text();

        //parses the request
        try {
            const response:any = JSON.parse(responseText);                

            //for error
            if(!res.ok) {              
                throw response;
            } 
            //for success
            else {
                return response;
            }
        } catch(e) { 
            // eslint-disable-next-line         
            throw `Could not parse respone: ${responseText}`;
        }
    }

    public async getCountries():Promise<ICountry[]> {

        const options = {
            method: 'post',
            headers: new Headers(),
            body: JSON.stringify({
                "query": "query { name: countries { name code }}"
            }),
        };

        options.headers.append('Content-Type', 'application/json');
        
        const fetchResult:Response = await fetch(ENDPOINT, options);
        const result = await this._getResponse(fetchResult);

        //remaps the result to something useful
        const ret:ICountry[] = [];
        (result.data.name as any[]).forEach((raw:any) => {
            const item:ICountry = {
                name: raw.name,
                code: raw.code
            };

            ret.push(item);
        });

        return ret;
    }
}
import { IAppService } from "./IAppService";
import { CountriesService } from "./CountriesService";

/**
 * The service that the app uses
 */
export let AppService:IAppService = new CountriesService();
/**
 * Sets the app with a new service
 * @param service The service to be used in the app
 */
export function setAppService(service:IAppService):void {
    AppService = service;
}
import React from "react";
import { ICountry } from "../../models/ICountry";
import { IState, useState } from "../../react-hooks/useState";
import { ICountryProviderProps } from "./ICountryProviderProps";
import { AppService } from "../../services/AppServiec";

export const CountryContext = React.createContext<IState<ICountry[]>|null>(null)

export function CountryProvider(props:React.PropsWithChildren<ICountryProviderProps>) {
    const _info:IState<ICountry[]> = useState<ICountry[]>(props.defaultValue || []);

    //loads in the countries info if not provided
    React.useEffect(() => {
        if(!props.defaultValue) {
            AppService.getCountries().then((info:ICountry[]) => {
                _info.set(info);
            });       
        }
    }, [_info, props.defaultValue]);
    

    return (
        <CountryContext.Provider value={_info}>
            { props.children }
        </CountryContext.Provider>
    );
}
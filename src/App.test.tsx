import { cleanup, render, screen } from '@testing-library/react';
import App from './App';
import { phrases } from './helpers/phrases';

afterEach(() => {
  cleanup();
});

it('Test APP rendering', () => {
  render(
    <App />
  );

  expect(screen.getByText(phrases.heading)).toBeInTheDocument();
});

import { act, cleanup, render, screen } from '@testing-library/react';
import { phrases } from '../../helpers/phrases';
import { setAppService } from '../../services/AppServiec';
import { DUMMY_DATA, DummyService } from '../../services/DummyService';
import { CountryProvider } from '../../providers/CountryProvider/CountryProvider';
import { CountryList } from './CountryList';
import { ICountry } from '../../models/ICountry';

afterEach(() => {
  cleanup();
});

beforeEach(() => {
    setAppService(new DummyService());    
});

it('Test Country component rendering with full data', async () => {

  render(
    <CountryProvider defaultValue={DUMMY_DATA}>
        <CountryList
            filter=''
        />
    </CountryProvider>
  );

  expect(screen.getByText(phrases.listCaption)).toBeInTheDocument();

  for(let i:number = 0; i < DUMMY_DATA.length; i++) {
    const country:ICountry = DUMMY_DATA[i];
    expect(screen.getByText(country.name)).toBeInTheDocument();
    expect(screen.getByText(country.code)).toBeInTheDocument();
  }
});

test('Test Country component rendering with filtered data', async () => {

    render(
      <CountryProvider defaultValue={DUMMY_DATA}>
          <CountryList
              filter='ee'
          />
      </CountryProvider>
    );

    expect(screen.getByText('Estonia')).toBeInTheDocument();
    expect(screen.getByText('EE')).toBeInTheDocument();
    
    for(let i:number = 0; i < DUMMY_DATA.length; i++) {
        const country:ICountry = DUMMY_DATA[i];
        if(country.code === 'EE') {
            continue;
        }

        expect(screen.queryByText(country.name)).toBeNull();
        expect(screen.queryByText(country.code)).toBeNull();
    }
});

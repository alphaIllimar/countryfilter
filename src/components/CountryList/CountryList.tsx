import React from "react";
import { ICountry } from "../../models/ICountry";
import { CountryContext } from "../../providers/CountryProvider/CountryProvider";
import { ICountryListProps } from "./ICountryListProps";
import { IState, useState } from "../../react-hooks/useState";
import styles from './CountryList.module.scss';
import { phrases } from "../../helpers/phrases";

export function CountryList(props:ICountryListProps) {

    const _getFilteredCountries = ():ICountry[] => {
        let ret:ICountry[];
        const filter = props.filter.trim().toUpperCase();
        if(!filter) {
            ret = _allCountries?.value || [];
        }
        else {
            ret = (_allCountries?.value || []).filter((c:ICountry) => c.code.startsWith(filter));
        }
        return ret;
    }

    const _allCountries:IState<ICountry[]>|null = React.useContext(CountryContext);
    const _filteredCountries:IState<ICountry[]> = useState(_getFilteredCountries());

    React.useEffect(() => {
        _filteredCountries.set(_getFilteredCountries());
    }, [props.filter, _allCountries?.value]);

    return (
        <table className={styles.list}>            
            <caption>{ phrases.listCaption }</caption>
            <tbody>
            {
                _filteredCountries.value.map((country:ICountry) => (
                    <tr key={`${country.code}_${country.name}`}>
                        <th>{country.name}</th>
                        <td>{country.code}</td>
                    </tr>
                ))
            }
            </tbody>
        </table>
    )
}
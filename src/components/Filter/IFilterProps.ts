export interface IFilterProps {
    onChange: (newFilter:string) => void;
}
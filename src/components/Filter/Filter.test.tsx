import { act, cleanup, render, screen } from '@testing-library/react';
import { phrases } from '../../helpers/phrases';
import Filter from './Filter';
import userEvent from '@testing-library/user-event';

afterEach(() => {
  cleanup();
});

it('Test Filter component', () => {
  let _filter:string = '';

  render(
    <Filter
        onChange={(filter:string) => {
            _filter = filter;
        }}
    />
  );

  const _inputText:string = 'ee';
  const _input:HTMLInputElement = screen.getByLabelText(phrases.code);
  const _button:HTMLButtonElement = screen.getByText(phrases.search);

  expect(_input).toBeInTheDocument();
  expect(_button).toBeInTheDocument();

  act(() => {
    userEvent.type(_input, _inputText);        
  });

  act(() => {
    userEvent.click(_button);
  });

  expect(_filter).toBe(_inputText);
});

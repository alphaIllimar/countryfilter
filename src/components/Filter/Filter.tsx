import { Button, Stack, TextField } from "@mui/material";
import { IFilterProps } from "./IFilterProps";
import { phrases } from "../../helpers/phrases";
import styles from './Filter.module.scss';
import { IState, useState } from "../../react-hooks/useState";

export default function Filter(props:IFilterProps) {
    const _filterValue:IState<string> = useState<string>('');

    return (
        <div className={styles.container}>
            <Stack direction={'row'}>
                <TextField 
                    placeholder={phrases.code}
                    value={_filterValue.value}
                    onChange={(e) => _filterValue.set(e.target.value)}
                    fullWidth
                    label={phrases.code}
                />
                <Button
                    onClick={() => props.onChange(_filterValue.value)}                   
                >
                    { phrases.search }
                </Button>
            </Stack>
        </div>
    )
}